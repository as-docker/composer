# PHP 8.x CLI - COMPOSER


## info
FROM php:cli
Composer version 2.x


## features
+ lightweight, installed `zip` and `git` only
+ no additional php dependencies (use composer with `--ignore-platform-reqs`)
+ using Your local SSH key
+ storing composer's cache locally
+ composer works as Your local user (on Linux, Mac, Windows)
+ [yake](https://yake.amsdard.io/) installed


## simple run
```
docker run --rm -it -v $(pwd):/opt -v ~/.ssh:/home/php/.ssh -v ~/.docker-composer:/home/php/.composer \
    amsdard/composer:2 composer #YPOUR-COMMAND-HERE
```

* set up `/home/php/.ssh` docker's location to use Your local SSH keys
* set up `/opt` docker's location as main project directory (with composer.json)
* set up `/home/php/.composer` docker's location to store composer temporary files


## global composer command
Add composer alias-command to Your `~/.bashrc` or `~/.zshrc`:

```
alias composer='docker run --rm -it -v $(pwd):/opt -v ~/.ssh:/home/php/.ssh -v ~/.docker-composer:/home/php/.composer amsdard/composer:2 composer'
```


## run using https://yake.amsdard.io

Yakefile
```
composer: "docker run --rm -it
          -v ~/.ssh:/home/php/.ssh
          -v $(pwd):/opt
          -v ~/.docker-composer:/home/php/.composer
          amsdard/composer:2 composer --working-dir=/opt $CMD"
```

samples
```
yake composer install
yake composer require -vv doctrine/mongodb-odm
```


## other approach for composer
You can use composer.phar script located directly in Your project, see:
https://hub.docker.com/r/amsdard/php/
