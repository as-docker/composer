#!/bin/bash

UNAME="php"
OPT_UID=`stat -c "%u" /opt`
OPT_GID=`stat -c "%g" /opt`
CMD_ROOT=("php-fpm" "${RUN_AS_ROOT}")

if [ ! -L /root ]; then
    mv /root /usr
    ln -s /home/$UNAME /root
fi

if [[ "$OPT_UID" -gt "0" && "$(grep -c -E "(^| )${1}( |$)" <<< "$CMD_ROOT")" -eq "0" ]]; then
    usermod -u $OPT_UID $UNAME >/dev/null 2>&1
    groupmod -g $OPT_GID $UNAME >/dev/null 2>&1
    export HOME=/home/$UNAME
    chown $UNAME:$UNAME -R $HOME >/dev/null 2>&1
    sudo -E -u $UNAME -g $UNAME $@
else
    exec "$@"
fi